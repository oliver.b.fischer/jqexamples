
ejq()
{
    echo "--[[ Input is: " json/$1
    echo 
    /bin/cat json/$1
    echo
    echo "--[[ Path is: " $2
    echo

    jq $2 json/$1

    echo
}
