# Examples demonstrating the features of ./jq

Usage examples of *./jq*, a flexible JSON command-line JSON processor.

You can find out more on ./jq at [the ./jq homepage](http://stedolan.github.io/jq/).
